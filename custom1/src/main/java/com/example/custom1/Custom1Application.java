package com.example.custom1;

import com.example.custom1.config.Payment;
import com.example.custom1.config.Payment2;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class Custom1Application implements CommandLineRunner {

	private final Payment p;
	private final Payment2 p2;

	public static void main(String[] args) {
		SpringApplication.run(Custom1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		p.test();
		p2.test();
	}
}
